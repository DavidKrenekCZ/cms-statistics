<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/statistics'], function (Router $router) {
	$router->get("dashboard", [
        'as' => 'admin.statistics.dashboard',
        'uses' => "DashboardController@dashboard",
        'middleware' => 'can:statistics.dashboard.index'
    ]);
    $router->post("ajax-data/{from?}/{to?}", [
        'as' => 'admin.statistics.ajax-data',
        'uses' => "DashboardController@ajaxData",
        'middleware' => 'can:statistics.dashboard.index'
    ]);
});
