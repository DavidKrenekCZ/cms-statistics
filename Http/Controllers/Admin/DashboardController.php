<?php

namespace Modules\Statistics\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Spatie\Analytics\Period;
use \Analytics;
use \Carbon\Carbon;

class DashboardController extends AdminBaseController {
    public function dashboard() {
        return view('statistics::admin.dashboard', [
        	"data" => Analytics::fetchTotalVisitorsAndPageViews(Period::create(\Carbon\Carbon::now()->addWeeks(-3), \Carbon\Carbon::now()))
        ]);
    }

    public function ajaxData($from=false, $to=false) {
    	$from = $from ? Carbon::createFromFormat("Y-m-d", $from) : Carbon::now()->addMonths(-1);
    	$to = $to ? Carbon::createFromFormat("Y-m-d", $to) : Carbon::now();

    	// If FROM date is after TO, switch the two dates
    	if ($from > $to) {
    		$fromVal = $from;
    		$from = $to;
    		$to = $fromVal;
    	}

    	if ($to > Carbon::now())
    		$to = Carbon::now();

    	$data = Analytics::fetchTotalVisitorsAndPageViews(Period::create($from, $to));

    	$data = $data->map(function($item) {
    		$item["date"] = $item["date"]->format("Y-m-d");
    		return $item;
    	});

    	return response()->json([
    		"data" 	=> $data,
    		"from" 	=> $from->format("d. m. Y"),
    		"to"	=> $to->format("d. m. Y")
    	]);
    }
}