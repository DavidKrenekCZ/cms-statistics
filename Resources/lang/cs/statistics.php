<?php

return [
	"permission" 		=> "Zobrazit statistiky",
	"title" 			=> "Statistiky",
	"chartDataLabel" 	=> [
		"visitors" 	=> "Návštěvníků",
		"pageViews" => "Zobrazení stránky"
	],
	"inputLabels" 		=> [
		"date from" => "Od",
		"date to"	=> "Do",
		"button"	=> "Ok"
	]
];