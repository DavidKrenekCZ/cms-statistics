<?php

return [
	"permission" 		=> "Browse statistics",
	"title" 			=> "Statistics",
	"chartDataLabel" 	=> [
		"visitors" 	=> "Visitors",
		"pageViews" => "Page views"
	],
	"inputLabels" 		=> [
		"date from" => "From",
		"date to"	=> "To",
		"button"	=> "Ok"
	]
];