@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('statistics::statistics.title') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('statistics::statistics.title') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header"></div>
                <div class="box-body" style="position: relative">
                    <!-- CONTROLS -->
                    <div class="statistics-controls">
                        <input type="text" class="datepicker statistics-input-from form-control" 
                            placeholder="{{ trans("statistics::statistics.inputLabels.date from") }}">
                        <input type="text" class="datepicker statistics-input-to form-control" 
                            placeholder="{{ trans("statistics::statistics.inputLabels.date to") }}">
                        <input type="button" class="statistics-input-button btn btn-primary" 
                            value="{{ trans("statistics::statistics.inputLabels.button") }}">
                    </div>

                    <!-- CHART -->
                    <div id="statistics-chart" style="height: 250px;"></div>
                </div>
                <div class="statistics-chart-loading">
                    <i class="fa fa-cog fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop

@push("css-stack")
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" integrity="sha256-WgFzD1SACMRatATw58Fxd2xjHxwTdOqB48W5h+ZGLHA=" crossorigin="anonymous" />
    <style type="text/css">
        .datepicker-dropdown {
            z-index: 1000 !important;
        }

        .statistics-chart-loading {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            text-align: center;
            z-index: 1001;
            font-size: 5rem;
            background: white;
        }

        .statistics-chart-loading .fa {
            position: absolute;
            top: calc(50% - 2.5rem);
            left: calc(50% - 2.5rem);
        }

        .statistics-controls {
            border-bottom: 1px solid #d2d6de;
            padding-bottom: 1rem;
            margin-bottom: 1rem;
            text-align: center;
        }

        .statistics-controls .datepicker {
            margin: 5px 0;       
        }

        @media (min-width: 768px) { /* desktop only */
            .statistics-controls .datepicker {
                width: 40%;
                display: initial;
            }

            .statistics-controls .statistics-input-button {
                margin-top: -3px;
            }
        }

        @media (min-width: 1080px) { /* bigger desktop only */
            .statistics-controls .datepicker {
                width: 20%;
                display: initial;
            }
        }
    </style>
@endpush

@push('js-stack')
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js" integrity="sha256-TueWqYu0G+lYIimeIcMI8x1m14QH/DQVt4s9m/uuhPw=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.cs.min.js" integrity="sha256-x5slhErTH7oK26gg11X6oD/V5Wu1rp5XD6Z2BlKkPZE=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        var chart = new Morris.Line({
            element: 'statistics-chart',
            data: [],
            xkey: 'date',
            ykeys: ['pageViews', 'visitors'],
            labels: [
                '{{ trans("statistics::statistics.chartDataLabel.pageViews") }}',
                '{{ trans("statistics::statistics.chartDataLabel.visitors") }}'
            ],
            xLabelFormat: function(d) {
                return d.getDate()+". "+(parseInt(d.getMonth())+1)+". "+d.getFullYear();
            },
            dateFormat: function(x) {
                var d = new Date(x);
                return d.getDate()+". "+(parseInt(d.getMonth())+1)+". "+d.getFullYear();
            }
        });

        function loadData(getDatesFromInputs) {
            $(".statistics-chart-loading").show();
            var appendToUrl = "";
            if (getDatesFromInputs) {
                appendToUrl = "/"+formatDateForAjax($(".statistics-input-from").data("datepicker").viewDate)+
                              "/"+formatDateForAjax($(".statistics-input-to").data("datepicker").viewDate);
            }
            $.ajax("{{ route("admin.statistics.ajax-data") }}"+appendToUrl, {
                data: {
                    _token: "{{ csrf_token() }}"
                },
                type: "post"
            }).done(function(d) {
                chart.setData(d.data);
                $(".statistics-input-from").datepicker("setDate", d.from);
                $(".statistics-input-to").datepicker("setDate", d.to);
            }).fail(function() {
                alert("Během načítání dat nastala chyba, opakujte akci.");
            }).always(function() {
                $(".statistics-chart-loading").hide();
            });
        }

        function formatDateForAjax(date) {
            return date.getFullYear()+"-"+(parseInt(date.getMonth())+1)+"-"+date.getDate();
        }

        $(function() {
            loadData(false);

            $('.datepicker').datepicker({
                format: "dd. mm. yyyy",
                language: "{{ locale() }}",
                startDate: new Date(1970, 0, 1),
                endDate: "today"
            });

            $(".statistics-input-button").click(loadData);
        });
    </script>
@endpush
