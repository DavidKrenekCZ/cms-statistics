## Installation
- Add a few things to arrays in `config/app.php`
``` php
	'aliases' => [
		// ...
		'Analytics' => Spatie\Analytics\AnalyticsFacade::class,
		// ...
	],

	'providers' => [
		// ...
		Spatie\Analytics\AnalyticsServiceProvider::class,
		// ...
	]
```
- Publish Analytics config file
``` php
php artisan vendor:publish --provider="Spatie\Analytics\AnalyticsServiceProvider"
```
- Get your credentials as described in https://github.com/spatie/laravel-analytics
- Edit credentials in **config/analytics.php**
- Done :)